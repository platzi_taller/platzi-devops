Summary
(Da el resumen del issue)

Steps to reproduce
(Pasos para reproducir el bug)

What is the current behavior?
(Cuál es el comportamiento actual)

What is the expected behavior?
(Cuál es el comportamiento esperado)